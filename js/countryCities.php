$("#reCountry").multiselect({
        selectedText: function(numChecked, numTotal, checkedItems){
            var selectedValues = new Array();
            for (var i = 0; i < checkedItems.length; i++) {
                if(checkedItems[i].value==10) checkedItems[i].value="<?php print __("Any"); ?>";
                selectedValues[i]=checkedItems[i].value;
            }
              return "<?php print __("Country");?>: " + selectedValues.join(", ");
           },
        height:'auto',
        multiple:false,
        checkAllText:'<?php print __("Check all"); ?>',
        uncheckAllText:'<?php print __("Uncheck all"); ?>'      
    });

<?php
 $query="select id,country from countries";
 $result=mysql_query($query);
 while($record = mysql_fetch_assoc($result)){
     $country=str_replace(" ", "_", $record['country']);
 ?>
 $('#town-<?php print $country; ?>').multiselect({
    selectedText: function(numChecked, numTotal, checkedItems){
            var selectedValues = new Array();
            for (var i = 0; i < checkedItems.length; i++) {
                if(checkedItems[i].value==10) checkedItems[i].value="<?php print __("Any"); ?>";
                selectedValues[i]=checkedItems[i].value;
            }
              return "<?php print __("Town/City");?>: " + selectedValues.join(", ");
           },
    selectedList: 1,
    height:'200px',
    multiple:false  
 });
 
 <?php } ?>
 
var country=$.trim($("#reCountry option:selected").val());
country=country.replace(" ","_");
$('#'+country).show(); 

$('#reCountry').change(function(){
 country=$.trim($("#reCountry option:selected").val()); 
 country=country.replace(" ","_");
 <?php
 $query="select id,country from countries";
 $result=mysql_query($query);
 while($record = mysql_fetch_assoc($result)){
     $country=str_replace(" ", "_", $record['country']);
 ?>
 $('#<?php print $country; ?>').hide();
 <?php } ?>
 $('#'+country).show(); 
    });

var country=$.trim($("#recountry2 option:selected").val());
country=country.replace(" ","_");
$('#listing-'+country).show();

$('#recountry2').change(function(){
 country=$.trim($("#recountry2 option:selected").val()); 
 country=country.replace(" ","_");
 <?php
 $query="select id,country from countries";
 $result=mysql_query($query);
 while($record = mysql_fetch_assoc($result)){
     $country=str_replace(" ", "_", $record['country']);
 ?>
 $('#listing-<?php print $country; ?>').hide();
 <?php } ?>
 $('#listing-'+country).show(); 
 });
 
$('#allcountries').sortable({
        update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').toString();
                $.get('countryShuffle.php', {order:newOrder , ptype:"updateCountries" , shuffle:"1"}, function(data){$('.result').html(data);});
        }
});
$("#allcountries").disableSelection();

$('.ui-state-default').click(function(){
 $.get('packageAction.php', {id:this.id , action:"show"}, function(data){$('.result').html(data);});   
});

$(".ui-state-default span.removeCountry").click(function(){
   var conf = confirm("Are you sure you want to delete this country?");
   if(conf == true){
   $.get('countryShuffle.php', {id:this.id , remove:"1"}, function(data){$('.result').html(data);  });
   $(this).parent().hide();
   }
});    

$(".ui-state-default span.removeTown").live('click', function(){ 
   var conf = confirm("Are you sure you want to delete this town/city?");
   if(conf == true){
   var country=$.trim($("#countriesList option:selected").val());  
   $.get('townsAction.php', {country:country , town:this.id , action:"remove"}, function(data){$('.result').html(data); alert(data);  });
   $(this).parent().hide();
   }
});

$("#countriesList").click(function(){
 var country=$.trim($("#countriesList option:selected").val());   
 $.get('townsAction.php', {country:country , action:"show"}, function(data){
     $('#alltowns').html(data);  
     $('#alltownslist').sortable({
        update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').join(':');
                $.get('townsAction.php', {country:country , order:newOrder , action:"shuffle"}, function(data){ $('.results').html(data); });
        }
});
     });
});

$("#addtown").click(function(){
   var country=$.trim($("#countriesList option:selected").val());
   var town=$.trim($("#townname").val());
   if(country!="" && town!=""){ 
   $.get('townsAction.php', {country:country , town:town , action:"add"}, function(data){
       $('#alltowns').html(data);  
       $('#alltownslist').sortable({
        update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').join(':');
                $.get('townsAction.php', {country:country , order:newOrder , action:"shuffle"}, function(data){ $('.results').html(data); });
        }
});
       });
   }
   else alert('Please select a country and specify town/city name.');
});    

