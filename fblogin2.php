<?php
error_reporting(-1);
ini_set('display_errors', 'On');
include("connect.inc.php");

require "Facebook2/FacebookRequest.php";
require "Facebook2/FacebookSession.php";
require "Facebook2/GraphObject.php";
require "Facebook2/GraphUser.php";
require "Facebook2/FacebookRequestException.php";
use Facebook\FacebookRequest;
use Facebook\FacebookSession;
use Facebook\GraphObject;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;

$social_name="facebook";

if(trim($_POST['myemail'])!="") finish_registration($social_name);
else fb_login($social_name);

function fb_login($social_name){
include("config.php"); 
$config = array();
$config['appId'] = $fb_app_id; 
$config['secret'] = $fb_app_secret; 
$fb_site_url = "http://" . $_SERVER['HTTP_HOST'] . preg_replace("#/[^/]*\.php$#simU", "/", $_SERVER["PHP_SELF"])."fblogin.php";
FacebookSession::setDefaultApplication($fb_app_id, $fb_app_secret);
$helper = new FacebookRedirectLoginHelper($fb_site_url);

try { $session = $helper->getSessionFromRedirect(); } 
    catch(FacebookRequestException $ex) { print "Facebook: $ex"; } 
    catch(\Exception $ex) { print "Exception: $ex"; }

if($session) {

  try {
    $fbReq=new FacebookRequest($session, 'GET', '/me');
    $user_profile = $fbReq->execute()->getGraphObject(GraphUser::className());
    login_process($user_profile,$social_name);
  } catch(FacebookRequestException $e) {

    echo "Exception occured, code: " . $e->getCode();
    echo " with message: " . $e->getMessage();
    error_log($e);
    $user = null;
  }   

}

// Login or logout url will be needed depending on current user state.
if ($user) {
	$logoutUrl = $facebook->getLogoutUrl();
} else {
	$login_url = $facebook->getLoginUrl(array('scope' => 'email'));
    //$loginUrl = $helper->getLoginUrl();
	header("Location: ".$login_url);
}

}


?>