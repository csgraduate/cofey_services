<?php 
include("config.php"); if($isThisDemo=="yes") exit; 
$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");
$query="select id,category from $categoryTable";
$result=mysql_query($query);
$i=0;

while($record = mysql_fetch_assoc($result)){
	$categories[$record['category']] = $record['id'];
	$catorder[$i]=$record['id']; $i++;
}

if (isset($_GET['order'])){
$order = explode(",",stripslashes(htmlspecialchars($_GET['order'], ENT_QUOTES, 'UTF-8')));
$bogusId=5000;
/*
 * First assign bogus id to each category so no conflict would occur when we update their order id
 */
foreach($catorder as $pointer => $orderid){
	mysql_query("update $categoryTable set id='$bogusId' where category='$order[$pointer]'");
	$bogusId++;
}

/*
 * Now assign the new order id
 */
foreach($catorder as $pointer => $orderid){
mysql_query("update $categoryTable set id='$orderid' where category='$order[$pointer]'");
}

}else {

?>

<!DOCTYPE html>
<html>
<head>
<title>Category Editor</title>
<style>
        .ui-sortable { list-style-type: none; margin: 20px 0; padding: 0; width: 60%; }
        .ui-sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; cursor: move; }
        .ui-sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<script type="text/javascript" language="javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery-ui-1.8.17.custom.min.js"></script>
<script>
$(function(){
	$('#allcategories').sortable({
        update: function(event, ui) {
                var newOrder = $(this).sortable('toArray').toString();
                $.get('categoriesEdit.php', {order:newOrder}, function(data){$('.result').html(data);});
        }
});
	$("#allcategories").disableSelection();
});
</script>
</head>
<body>

<ul id="allcategories">
<?php 
foreach($categories as $catname => $catid){
        echo '<li class="ui-state-default" id="'.$catname.'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'.$catname.'</li>';
}
?>
</ul>
<br />
<div class='result'></div>
</body>
</html>
<?php 
}
?>