<?php include("config.php"); include_once("functions.inc.php");
if($_SESSION["memtype"]==9){
?>
<b>Specify Subcategory Name in English:</b><br /><br />
<input type='text' name='subcategoryname' id='subcategoryname' size='35' /><br /><br />
<?php if(strtolower($redefaultLanguage)!="english"){ ?>
<b>Specify Subcategory Name in <?php print $redefaultLanguage; ?>:</b><br /><br />
<input type='text' name='subcategorytrans' id='subcategorytrans' size='35' /><br />
<?php } ?>    
<input type='button' class='btn btn-large' id='addsubcategory' value='Add Subcategory' />
<br /><br /><b>Existing Subcategories</b><br /><?php if(strtolower($redefaultLanguage)!="english") print "(If any subcategory name below doesn't appear in $redefaultLanguage, then add its translation on <b>language tags</b> page)"; ?><br />
<?php

$category=$_GET['category'];
$subcategory=$_GET['subcategory'];
$action=$_GET['action'];

$con=mysql_connect($host,$username,$password) or die("Could not connect. Please try again.");
mysql_select_db($database,$con);
mysql_query("SET NAMES utf8");

$query="select id,subcategories from $categoryTable where category='$category'";
$result=mysql_query($query);

$row=mysql_fetch_assoc($result);

if(trim($row['subcategories'])=="") $allSubcategories=$subcategory;
else $allSubcategories=$row['subcategories'].":::".$subcategory;
//print $allTowns;
if($action!="shuffle" && $action!="remove"){
$subcategoryArray=explode(":::",$allSubcategories);
print '<ul id="allsubcatlist" style="list-style: none; ">';
foreach($subcategoryArray as $id => $subcategoryName){
    if(trim($subcategoryName)!=""){
    ?>
 <li class="ui-state-default" id="<?php print $subcategoryName; ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php print __($subcategoryName); ?> 
 <span class="removeSubcategory" id="remove-<?php print $subcategoryName; ?>" style="font-size:10px; margin-left:50px; text-decoration:underline;">Remove</span></li>
<?php
    }
}
print '</ul>';
}

if($action=="add" && $isThisDemo!="yes"){
    
if(trim($allSubcategories)!=""){
include_once("functions.inc.php");
    
if(strtolower($redefaultLanguage)=="english")$subcategorytrans=$subcategory;    
else $subcategorytrans=$_GET['subcategorytrans'];
$subcategory = friendlyUrl($subcategory," ");
print "adding $subcategory<br />";
$qr00="select * from $languageTable where translation='$subcategorytrans' and language='$redefaultLanguage'"; 
$result00=mysql_query($qr00);
if(mysql_num_rows($result00)<1){    
$qr2="insert into $languageTable (keyword,translation,language) values ('$subcategory','$subcategorytrans','$redefaultLanguage')";
$result2=mysql_query($qr2);
if($result2){
$qr1="update $categoryTable set subcategories='$allSubcategories' where category='$category';";
$result1=mysql_query($qr1);
}else $setMessage="<p align='center'>Translation couldn't be added. Please try again.<p>"; 
 }else{
  $row00=mysql_fetch_assoc($result00);   
  print "<p align='center'>Same translation already exist for the keyword <b>'".$row00['keyword']."'</b>. Please use a unique translation.<p>";   
 }
          
        
}

if(!$result1) print "<br /><span class='alert alert-error'>Subcategory couldn't be added. ".mysql_error()."</span>";
}

if($action=="remove"  && $isThisDemo!="yes"){
    $subcategory=str_replace("remove-", "", $subcategory);
    $newSubcategoryList=str_replace("$subcategory:::", "", $row['subcategories']);
    $newSubcategoryList=str_replace(":::$subcategory", "", $newSubcategoryList);
    $newSubcategoryList=str_replace("$subcategory", "", $newSubcategoryList);
    
    $query0="delete from $languageTable where keyword='".$subcategory."' and language='$redefaultLanguage';";
    $result0=mysql_query($query0);

    $qr1="update $categoryTable set subcategories='$newSubcategoryList' where category='$category';";
    $result1=mysql_query($qr1);
    if($result1) print "<span class='alert alert-success'>Subcategory deleted.</span>";
    else print "<span class='alert alert-error'>Subcategory couldn't be deleted. ".mysql_error()."</span>";
}

if($action=="shuffle"  && $isThisDemo!="yes"){
   $order=trim($_GET['order']); 
    if($order!=""){
    $qr1="update $categoryTable set subcategories='$order' where category='$category';";
    $result1=mysql_query($qr1);
    }

if($result1) print "<br /><span class='alert alert-success'>Subcategory order updated.</span>";
else print "<br /><span class='alert alert-error'>Subcategory order couldn't be updated. ".mysql_error()."</span>";
}

}
?>