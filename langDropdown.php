<dl id="langselector" class="langdropdown">
        <dt><a href="#"><span>Select language</span></a></dt>
        <dd>
            <ul style="z-index:100;">
<li><a href="#">﻿Arabic<span class="value">Arabic</span></a></li>
<li><a href="#">Bulgarian<span class="value">Bulgarian</span></a></li>
<li><a href="#">Catalan<span class="value">Catalan</span></a></li>
<li><a href="#">Chinese Simplified<span class="value">Chinese Simplified</span></a></li>
<li><a href="#">Czech<span class="value">Czech</span></a></li>
<li><a href="#">Danish<span class="value">Danish</span></a></li>
<li><a href="#">Dutch<span class="value">Dutch</span></a></li>
<li><a href="#">English<span class="value">English</span></a></li>
<li><a href="#">Estonian<span class="value">Estonian</span></a></li>
<li><a href="#">Finnish<span class="value">Finnish</span></a></li>
<li><a href="#">French<span class="value">French</span></a></li>
<li><a href="#">German<span class="value">German</span></a></li>
<li><a href="#">Greek<span class="value">Greek</span></a></li>
<li><a href="#">Haitian Creole<span class="value">Haitian Creole</span></a></li>
<li><a href="#">Hebrew<span class="value">Hebrew</span></a></li>
<li><a href="#">Hindi<span class="value">Hindi</span></a></li>
<li><a href="#">Hungarian<span class="value">Hungarian</span></a></li>
<li><a href="#">Indonesian<span class="value">Indonesian</span></a></li>
<li><a href="#">Italian<span class="value">Italian</span></a></li>
<li><a href="#">Japanese<span class="value">Japanese</span></a></li>
<li><a href="#">Korean<span class="value">Korean</span></a></li>
<li><a href="#">Latvian<span class="value">Latvian</span></a></li>
<li><a href="#">Lithuanian<span class="value">Lithuanian</span></a></li>
<li><a href="#">Norwegian<span class="value">Norwegian</span></a></li>
<li><a href="#">Polish<span class="value">Polish</span></a></li>
<li><a href="#">Portuguese<span class="value">Portuguese</span></a></li>
<li><a href="#">Romanian<span class="value">Romanian</span></a></li>
<li><a href="#">Russian<span class="value">Russian</span></a></li>
<li><a href="#">Slovak<span class="value">Slovak</span></a></li>
<li><a href="#">Slovenian<span class="value">Slovenian</span></a></li>
<li><a href="#">Spanish<span class="value">Spanish</span></a></li>
<li><a href="#">Swedish<span class="value">Swedish</span></a></li>
<li><a href="#">Thai<span class="value">Thai</span></a></li>
<li><a href="#">Turkish<span class="value">Turkish</span></a></li>
<li><a href="#">Ukrainian<span class="value">Ukrainian</span></a></li>
<li><a href="#">Vietnamese<span class="value">Vietnamese</span></a></li>
                        </ul>
        </dd>
    </dl>
 