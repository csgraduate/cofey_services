<?php
/*
*     Author: Ravinder Mann
*     Email: ravi@codiator.com
*     Web: http://www.codiator.com
*     Release: 1.4
*
* Please direct bug reports,suggestions or feedback to :
* http://www.codiator.com/contact/
*
* Script: Classified made easy. Please respect the terms of your license. More information here: http://www.codecanyon.net/licenses
*
*/

/*
 * Filters data through the functions registered for "viewFullListing" hook.
 * Passes an array of aruguments. $vargs[0] is the entire html listing data.
 * $vargs[1] is the entire row of data fetched from database for a particular listing id
 */
 

$vargs[0]=viewFullListing($viewListingRow,$mem_id,$showMoreListings);
$vargs[1]=$viewListingRow;

$vdata=call_plugin("viewFullListing",$vargs);
print $vdata[0];

function viewFullListing($viewListingRow,$mem_id,$showMoreListings=""){
    global $ptype, $reid;
    include("config.php");
ob_start();   
$row=$viewListingRow;
$region=htmlspecialchars(trim($_GET["region"]), ENT_QUOTES, 'UTF-8');

if($row['user_id']!="oodle"){
$reqr1="select price from $categoryTable where category='".$row['category']."'";
$resultre1=mysql_query($reqr1);
$categoryRow=mysql_fetch_assoc($resultre1);

$rePicArray=explode("::",$row['pictures']);
$totalRePics=sizeof($rePicArray);
if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures;
if($row['show_image']=="yes"){
	$qr1="select photo from $rememberTable where id='".$row['user_id']."'";
	$result1=mysql_query($qr1);
	$row1=mysql_fetch_assoc($result1);
	$poster_pic=$row1['photo'];	
}
if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true){
    $contactImageClause=" style='float:left;' ";
}
if(trim($poster_pic)!="") $contact_image="<div class='recontact_image' $contactImageClause><img src='uploads/".$poster_pic."' height='125' alt='' /></div>";
else $contact_image="<div class='recontact_image' $contactImageClause><img src='images/identity.png' height='128' alt='' /></div>";

}else{
	//include_once("functions.inc.php");
	$rePicArray=explode("::",$row['pictures']); 
	$totalRePics=sizeof($rePicArray);
	if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures;
	$poster_pic=$row['photo'];	
	if(trim($poster_pic)!="") $contact_image="<div class='recontact_image'><img src='".$poster_pic."' height='125' alt='' /></div>";
	else $contact_image="<div class='recontact_image'><img src='images/identity.png' height='128' alt='' /></div>";
	
	require_once('geoplugin.class.php');
	$geoplugin = new geoPlugin();
	$geoplugin->locate();
	$vCountry=$geoplugin->countryName;
	$defaultCurrency=getOodleCurrency($vCountry,$defaultCurrency);
}

if($row['relistingby']=="owner") $row['relistingby']=__("Individual");
if($row['relistingby']=="reagent") $row['relistingby']=$row['listing_by_other'];
if($row['relistingby']=="") $row['relistingby']=__("Individual");
if($row['price']==0)$row['price']="";
if($row['resize']==0)$row['resize']="";
$row['price']=number_format($row['price']);

$_SESSION['currency_before_price']=$currency_before_price;

function printAttribute($attribute,$tag,$defaultCurrency=""){
   include_once("functions.inc.php");   
   if($attribute!=""){
     if($_SESSION['currency_before_price']) $full_attribute=$defaultCurrency.__($attribute);
     else  $full_attribute=__($attribute)." ".$defaultCurrency;  
     if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) $attrClause=" style='float:left;' ";
     print "<tr><td><b>".__($tag).":</b> <span class='attr_value' $attrClause>".$full_attribute."</span></td></tr>";
   } 
}

?>

<div id='rememberAction'>
<table id='resultTable'>
<tr class='headRow1'><td colspan='2'><b><?php print __($row['category'])." - ".__($row['subcategory'])." - ".$row['city']." (".__("Listing id")." #".$row['id'].")"; ?></b>
<?php if($row['listing_type']==2){ print "<span class='featuredlisting label label-primary'>".$relanguage_tags["Featured"]."</span>"; } print hasVisited($row['id']); ?>
<div class="pull-right" id='closeMapListing' style="cursor:pointer;"><img src='images/fancy_close.png' alt='close' height='20' /></div>
</td></tr>
<tr>
<td style="vertical-align:top; width:40%;">
 
<table id='listing_attributes'>
    <?php 
    $full_address="";
    if($row['address']!="") $full_address=$row['address'];
    if($row['city']!="") $full_address=$full_address.", ".$row['city'];
    if($row['state']!="") $full_address=$full_address.", ".$row['state'];
    if($row['postal']!="") $full_address=$full_address.", ".$row['postal'];
    if($row['country']!="") $full_address=$full_address.", ".$row['country'];
    $full_address=trim($full_address,',');
printAttribute($row['category'],"Category");
printAttribute($row['subcategory'],"Sub Category");
printAttribute($row['relistingby'],"Listing by");
printAttribute(number_format($row['price']),"Price",$defaultCurrency);
printAttribute($row['dttm_modified'],"Date Listed");
?>
 <tr><td class='address_attr'><b>Address:</b> <span class='attr_value' <?php if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) print " style='float:left;' "; ?> ><?php print $full_address; ?></span></td></tr>
</table>

</td>
<td style="vertical-align:top;">
<?php 
$_SESSION["reid"]=$row['id'].":".$_SESSION["reid"];
if($totalRePics>=1) { ?>
<div id='listingImages'>
<?php 
$imgRowCount=0;
for($imgCount=0;$imgCount<$totalRePics;$imgCount++){ 
if(trim($rePicArray[$imgCount])!=""){
print "<span><a rel='prettyPhoto[".$row['id']."]'  data-fancybox-group='listgallery' href='$rePicArray[$imgCount]'><img src='timthumb.php?w=77&amp;src=$rePicArray[$imgCount]' alt='listing image' /></a></span>";	
}
}

?>
</div>
<?php }
$allMarkedreid=explode(":",$_SESSION["marked_reid"]); 
?>

<div id='listingButtons'>
<?php if(in_array($row['id'],$allMarkedreid)){ ?>
<div id='reAlreadyMarkedListing'><span  class='btn btn-primary' disabled="disabled" title='<?php print $relanguage_tags["This listing has been liked by you"];?>.'><?php print $relanguage_tags["Liked"]." ".$relanguage_tags["Listing"];?></span></div>
<?php }else{ ?>
<div id='reMarkedListing'><span class='btn btn-default' title='<?php print $relanguage_tags["Mark this listing to find it easily in future"];?>.'  onclick="infoResults('<?php print $row['id']; ?>',8,'reMarkedListing');"><?php print $relanguage_tags["Like Listing"];?></span></div>
<?php } ?>

<?php if($row['user_id']!="oodle"){ ?><a title='<?php print $relanguage_tags["Click above to send a message to the poster of this listing"]; ?>' href='contactPoster.php?reid=<?php print $row['id']; ?>' class='btn btn-default listingcontact'><?php print __("Contact");?></a>
<?php
$ip=$_SERVER["REMOTE_ADDR"];
$listing_id=$row['id'];
$fqr="select * from flagging where ip='$ip' and listing_id='$listing_id'";
$fresult=mysql_query($fqr); 
if(mysql_num_rows($fresult) > 0){
    $reportButton=__("Reported");
    $reportClause=' disabled="disabled" ';
}else{
    $reportButton=__("Report this");
    $reportClause="";
}
if($row['user_id']!="oodle"){
?>
<div id='reFlaggedListing'><span onclick="infoResults('<?php print $row['id']; ?>',28,'reFlaggedListing');" class="btn btn-danger listingflag" title="" data-original-title="<?php print __("Flag this listing"); ?>" <?php print $reportClause; ?> ><?php print $reportButton; ?></span></div>
<?php } } ?>

</div>

</td>
</tr>

<tr id='reDescriptionRow'>
<td colspan='2'><div id='listingAllowedThings'><?php print $reAllowedThings; ?></div>
<div class='listingItem'><b><span class='reListingHeadline'><?php print $row['headline']; ?></span></b></div>
<div class='listingItem'><b><?php print $relanguage_tags["Description"];?>:</b><br /><div class='reListingDescription'><?php print nl2br($row['description']); ?>
<br /><br />
<?php if($row['user_id']=="oodle") print "<b>".__("More Information").":</b> <a href='".$row['url']."' target='_blank'>".__("Here")."</a>"; ?>
</div></div>
<?php if($ptype=="viewFullListing"){ ?>
<div class='listingItem'><b><?php print $relanguage_tags["Location on map"];?>:</b></div>
<div id='reListingOnMap'></div>
<?php } ?>
<?php if($row['contact_name']!="" || $row['contact_phone']!="" || $row['contact_email']!="" || $row['contact_website']!="" || $row['contact_address']!=""){ ?>
<div class='reContactInformation alert alert-warning'><b><?php print $relanguage_tags["Contact Information"];?></b>
<?php print $contact_image; if($row['contact_name']!=""){ ?><div class='recontact_info'><br /><b><?php print $relanguage_tags["Name"];?>:</b> <?php print $row['contact_name']; }
if($row['contact_phone']!="") print "<br /><b>".$relanguage_tags["Phone"]." :</b> ".$row['contact_phone'];
if($cladmin_settings['listingemail']=="yes") print "<br /><b>".$relanguage_tags["Email"].":</b> ".$row['contact_email'];
if($row['contact_website']!="") print "<br /><b>".$relanguage_tags["Website"].":</b> <a href='".$row['contact_website']."' target='_blank'>".$row['contact_website']."</a>";
if($row['contact_address']!="") print "<br /><b>".$relanguage_tags["Address"]."</b><br />".nl2br($row['contact_address']); 

?></div></div>
<?php } ?>
<div class='listingButtons' style="margin-bottom:40px;"><?php 
if(isset($_SESSION["memtype"]) && trim($ppemail)!="" && $featuredduration>0 && $featuredprice>0 && $row['listing_type']!=2 && $row['user_id']!="oodle") featuredButton($row['user_id'],$mem_id,$row['id']); 
showMemberNavigation($row['user_id'],$mem_id,$row['id'],2); 
if(isset($_SESSION["memtype"]) && trim($ppemail)!="" && $featuredduration>0 && $featuredprice>0  && $row['listing_type']!=2 && $row['user_id']!="oodle"){
?>
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<?php } ?>
</div>
</td>
</tr>

</table>
</div>

<?php if($showMoreListings!="no"){ ?>
<h3 class='reHeading1'><?php print $relanguage_tags["Similar listings based on your search criteria"];?></h3>
<div id='reResults2'></div>
<?php  } 
return ob_get_clean();
}
?>