<?php require_once('recaptcha/recaptchalib.php'); ?>
<table id='resultTable'><tr><td class='headRow1'><h3><?php print $relanguage_tags["Contact us"];?></h3></td></tr>
<tr><td class='spltd'>

<?php if(trim($contactaddress)!="") print "<br /><h3 align='center'>".$relanguage_tags["Address"]."</h3><p align='center'>".nl2br($contactaddress)."</p>"; ?>
<br />
<div id='contactpage'>
<?php if(trim($contactformemail)!=""){ ?>
<h3 align='center'><?php print $relanguage_tags["Send us a message"];?></h3>
<form name='contactForm' method='post' action='sendMessage.php'>
        <table align='center' width='100%'>
        
        <tr><td><b><?php print $relanguage_tags["Name"];?>:</b></td><td class='infield'><input type='text' size='30' id='visitor_name' name='visitor_name' class='col-xs-10 col-sm-10 col-md-10 col-lg-10'></td></tr>
        <tr><td><b><?php print $relanguage_tags["Email"];?>:</b></td><td  class='infield'><input type='text' size='30' id='visitor_email' name='visitor_email' class='col-xs-10 col-sm-10 col-md-10 col-lg-10'></td></tr>
        <tr><td><b><?php print $relanguage_tags["Message"];?></b></td><td class='infield'><textarea name='visitor_message' id='visitor_message' cols='37' rows='7' class='col-xs-10 col-sm-10 col-md-10 col-lg-10'></textarea></td></tr>
        <?php if(trim($reCaptchaPrivateKey)!="" && trim($reCaptchaPublicKey)!=""){ ?>
        <tr><td></td><td class='infield'><?php echo recaptcha_get_html($reCaptchaPublicKey); ?></td></tr>
        <?php } ?>
        <tr><td colspan='2' align='center'><input type='hidden' name='reid' value='contactuspage' /><input type='hidden' name='debug' value='yes' />
        <input type='submit' class='btn btn-lg btn-primary' value='<?php print $relanguage_tags["Submit"];?>' id='visitor_submit' /></td></tr>
        </table>        
</form>
<?php }else print "<p align='center'>Please specify contact us email under 'Contact us' tab on 'Admin options' page and a contact form would appear here.</p><br /><br />"; ?>
</div>
</td></tr></table>
