﻿Arabic:
منوعات الرومانسية
Bulgarian:
Misc романтика
Catalan:
Varis romanç
Chinese Simplified:
Misc 浪漫
Czech:
Různé Romance
Danish:
Misc Romance
Dutch:
Misc Romance
English:
Misc Romance
Estonian:
Misc Romance
Finnish:
Misc Romance
French:
Misc Romance
German:
Misc-Romantik
Greek:
Misc Αισθηματική
Haitian Creole:
Romans misc
Hebrew:
רומנטיקה Misc
Hindi:
विविध रोमांस
Hmong Daw:
Misc Romance
Hungarian:
Misc romantika
Indonesian:
Misc Romance
Italian:
Misc Romance
Japanese:
その他のロマンス
Korean:
기타 로맨스
Latvian:
Misc Romance
Lithuanian:
Misc romanų
Norwegian:
Misc romantikk
Polish:
Misc Romance
Portuguese:
Misc Romance
Romanian:
Misc Romance
Russian:
MISC Романс
Slovak:
Misc Romance
Slovenian:
Misc romanski
Spanish:
Romance de Misc
Swedish:
Misc Romance
Thai:
คาบาเร่ต์ Romance
Turkish:
Misc Romance
Ukrainian:
Різне романтика
Vietnamese:
Misc Romance