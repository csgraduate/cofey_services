﻿Arabic:
الموارد البشرية
Bulgarian:
Човешки ресурси
Catalan:
Recursos humans
Chinese Simplified:
人力资源
Czech:
Lidské zdroje
Danish:
Menneskelige ressourcer
Dutch:
Human Resources
English:
Human Resources
Estonian:
Inimressursside
Finnish:
Henkilöresurssit
French:
Ressources humaines
German:
Personalwesen
Greek:
Ανθρώπινοι πόροι
Haitian Creole:
Resous Imèn
Hebrew:
משאבי אנוש
Hindi:
मानव संसाधन
Hmong Daw:
Human Resources
Hungarian:
Emberi erőforrások
Indonesian:
Sumber daya manusia
Italian:
Risorse umane
Japanese:
人事管理
Korean:
인적 자원
Latvian:
Cilvēkresursu
Lithuanian:
Žmogiškųjų išteklių
Norwegian:
Menneskelige ressurser
Polish:
Zasoby ludzkie
Portuguese:
Recursos humanos
Romanian:
Resurse umane
Russian:
Людские ресурсы
Slovak:
Ľudské zdroje
Slovenian:
Človeških virov
Spanish:
Recursos humanos
Swedish:
Mänskliga resurser
Thai:
ทรัพยากรบุคคล
Turkish:
İnsan kaynakları
Ukrainian:
Людських ресурсів
Vietnamese:
Nguồn nhân lực