﻿Arabic:
الأمن
Bulgarian:
Защита
Catalan:
Seguretat
Chinese Simplified:
安全
Czech:
Bezpečnost
Danish:
Sikkerhed
Dutch:
Veiligheid
English:
Security
Estonian:
Turvalisus
Finnish:
Suojaus
French:
Sécurité
German:
Sicherheit
Greek:
Ασφαλείας
Haitian Creole:
Sekirite
Hebrew:
אבטחה
Hindi:
सुरक्षा
Hmong Daw:
Kev ruaj ntseg
Hungarian:
Biztonsági
Indonesian:
Keamanan
Italian:
Sicurezza
Japanese:
セキュリティ
Korean:
보안
Latvian:
Drošības
Lithuanian:
Saugumo
Norwegian:
Sikkerhet
Polish:
Zabezpieczenia
Portuguese:
Segurança
Romanian:
Securitate
Russian:
Безопасность
Slovak:
Bezpečnosť
Slovenian:
Varnost
Spanish:
Seguridad
Swedish:
Säkerhet
Thai:
รักษาความปลอดภัย
Turkish:
Güvenlik
Ukrainian:
Безпеки
Vietnamese:
An ninh