﻿Arabic:
خدمة العملاء
Bulgarian:
Обслужване на клиенти
Catalan:
Servei al client
Chinese Simplified:
客户服务
Czech:
Služby zákazníkům
Danish:
Kundeservice
Dutch:
Klantenservice
English:
Customer Service
Estonian:
Klienditeenindaja
Finnish:
Asiakaspalvelu
French:
Service à la clientèle
German:
Kundenservice
Greek:
Εξυπηρέτηση πελατών
Haitian Creole:
Kliyan sèvis
Hebrew:
שירות לקוחות
Hindi:
ग्राहक सेवा
Hmong Daw:
Neeg Service
Hungarian:
Ügyfélszolgálat
Indonesian:
Layanan pelanggan
Italian:
Servizio clienti
Japanese:
カスタマー サービス
Korean:
고객 서비스
Latvian:
Klientu apkalpošana
Lithuanian:
Klientų aptarnavimo
Norwegian:
Kundeservice
Polish:
Biuro obsługi klienta
Portuguese:
Atendimento ao cliente
Romanian:
Serviciul clienţi
Russian:
Обслуживание клиентов
Slovak:
Zákaznícky servis
Slovenian:
Customer Service
Spanish:
Servicio al cliente
Swedish:
Kundtjänst
Thai:
บริการลูกค้า
Turkish:
Müşteri Hizmetleri
Ukrainian:
Обслуговування клієнтів
Vietnamese:
Dịch vụ khách hàng