﻿Arabic:
إضافة فئة فرعية
Bulgarian:
Добавяне на под категория
Catalan:
Afegir la categoria Sub
Chinese Simplified:
添加子类别
Czech:
Přidat dílčí kategorie
Danish:
Tilføje underkategori
Dutch:
Subcategorie toevoegen
English:
Add Sub Category
Estonian:
Sub kategooria lisamine
Finnish:
Lisää alaluokka
French:
Ajouter catégorie Sup
German:
Fügen Sie Sub-Kategorie
Greek:
Προσθήκη υπο κατηγορία
Haitian Creole:
Ajoute metwo kategori
Hebrew:
הוספת קטגוריית משנה
Hindi:
उप श्रेणी जोड़ें
Hungarian:
Alkategória hozzáadása
Indonesian:
Menambahkan Sub Kategori
Italian:
Aggiungere la sottocategoria
Japanese:
サブカテゴリを追加します。
Korean:
하위 범주 추가
Latvian:
Pievienot Sub kategoriju
Lithuanian:
Pridėti Sub kategorijos
Norwegian:
Legg til underkategori
Polish:
Dodawanie kategorii Sub
Portuguese:
Adicionar Sub categoria
Romanian:
Adaugă Sub categoria
Russian:
Добавить Подкатегория
Slovak:
Add Sub kateg├│riu
Slovenian:
Dodati Sub kategorije
Spanish:
Añadir categoría Sub
Swedish:
Lägga till underkategori
Thai:
เพิ่มประเภทย่อย
Turkish:
Alt Kategori Ekle
Ukrainian:
Додати Підкатегорії Категорія
Vietnamese:
Thêm danh mục phụ