﻿Arabic:
الأعمال التجارية الصغيرة
Bulgarian:
Малък бизнес
Catalan:
Small business
Chinese Simplified:
小型企业
Czech:
Malé firmy
Danish:
Small business
Dutch:
Klein bedrijf
English:
Small business
Estonian:
Small business
Finnish:
Small business
French:
Petites entreprises
German:
Kleine Unternehmen
Greek:
Μικρές επιχειρήσεις
Haitian Creole:
Ti biznis
Hebrew:
עסקים קטנים
Hindi:
लघु व्यवसाय
Hmong Daw:
Cov lag luam me
Hungarian:
Small business
Indonesian:
Usaha kecil
Italian:
Piccole imprese
Japanese:
中小企業
Korean:
중소 기업
Latvian:
Mazo uzņēmumu
Lithuanian:
Smulkaus verslo
Norwegian:
Småbedrifter
Polish:
Mała firma
Portuguese:
Pequenas empresas
Romanian:
Small business
Russian:
Малый бизнес
Slovak:
Malé podniky
Slovenian:
Small business
Spanish:
Pequeñas empresas
Swedish:
Småföretag
Thai:
ธุรกิจขนาดเล็ก
Turkish:
Küçük işletme
Ukrainian:
Малий бізнес
Vietnamese:
Doanh nghiệp nhỏ