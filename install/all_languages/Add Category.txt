﻿Arabic:
إضافة فئة
Bulgarian:
Добавяне на категория
Catalan:
Afegir la categoria
Chinese Simplified:
添加类别
Czech:
Přidat kategorii
Danish:
Tilføj kategori
Dutch:
Categorie toevoegen
English:
Add Category
Estonian:
Kategooria lisamine
Finnish:
Lisää luokka
French:
Ajouter catégorie
German:
Kategorie hinzufügen
Greek:
Προσθέσετε κατηγορία
Haitian Creole:
Ajoute kategori
Hebrew:
הוספת קטגוריה
Hindi:
श्रेणी जोड़ें
Hungarian:
Kategória hozzáadása
Indonesian:
Tambahkan Kategori
Italian:
Aggiungi Categoria
Japanese:
カテゴリを追加します。
Korean:
범주 추가
Latvian:
Pievienot kategoriju
Lithuanian:
Įtraukti kategoriją
Norwegian:
Legge til kategori
Polish:
Dodawanie kategorii
Portuguese:
Adicionar categoria
Romanian:
Adăugare categorie
Russian:
Добавить категорию
Slovak:
Pridanie kategórie
Slovenian:
Dodaj kategorijo
Spanish:
Agregar categoría
Swedish:
Lägga till kategori
Thai:
เพิ่มประเภท
Turkish:
Kategori Ekle
Ukrainian:
Додавання категорії
Vietnamese:
Thêm thể loại